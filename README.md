# FEWER Common Alerting Protocol (CAP) Templates

The repository contains the templates that are used in the CAP installation for each territory supported by Fisheries Early Warning and Emergency Response (FEWER).

## Folder Structure

The repository contains the area templates (areaTemplates) and the message templates (messageTemplates)


## Supported Countries

The following countries are supported by FEWER:

| CODE | COUNTRY                       |
|------|-------------------------------|
| AG   | Antigua and Barbuda           |
| BB   | Barbados                      |
| DM   | Dominica                      |
| GD   | Grenada                       |
| KN   | St Kitts and Nevis            |
| LC   | Saint Lucia                   |
| TT   | Trinidad and Tobago           |
| VC   | St Vincent and the Grenadines |

<sub>Table was generated using [Tables Generator](https://www.tablesgenerator.com/markdown_tables)</sub>


## Area Template Generation

Inside of the area templates directory, the "rawkml" folder contains the KML files for the areas within the country. It also contains a python script called "convert.py" that will generate individual area templates from the country kml file developed on My Maps on the Google Maps platform.

To update the maps use the following steps:

1. Create virtual and activate environment

```bash
virtualenv venv --no-site-packages
source venv/bin/activate
```

2. Install requirements

```bash
pip install -r requirements.txt
```

3. Navigate to code and run converter

```bash
cd areaTemplates\rawkml
python convert.py
```